// employeeAction.js

import * as actionTypes from './actionTypes';

export const createEmployee = (employees) => {
  return {
    type: actionTypes.CREATE_NEW_EMPLOYEE,
    employees: employees
  }
};

export const deleteEmployee = (id) => {
  return {
    type: actionTypes.REMOVE_EMPLOYEE,
    id: id
  }
};

export const updateEmployee = (employees, index) => {
  return {
    type: actionTypes.UPDATE_EMPLOYEE,
    employees: employees,
    index: index
  }
};

