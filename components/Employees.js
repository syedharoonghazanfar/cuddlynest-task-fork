import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, Modal, Button, TextInput } from 'react-native';
import { connect } from 'react-redux';
import Details from './Details'

class Employees extends Component {

  handleOpen = (visible, index) => {

    this.setState(
      { detailsVisible: visible });
    this.setState(
      { id: index });
  }

  searchUpdated = (term) => {
    var val = term.substr(0, 20);
    this.setState({ searchTerm: val });
  }

  handleViewFromChild=(val)=>{
    console.log(val);
    this.setState(
      { detailsVisible: val });
  }
  
  state = {
    detailsVisible: false,
    id: 0,
    searchTerm: ''
  }
  render() {
    var datas = this.props.employees.filter(
      (employee) => {
        return (employee.firstname + employee.lastname).toLowerCase().indexOf(this.state.searchTerm.toLowerCase()) !== -1;
      }
    )
    // var datas= this.props.employees
    const listItems = datas.map((d, index) => <Text style={styles.edit} onPress={() => { this.handleOpen(true, index) }} key={index}>{d.firstname + ' ' + d.lastname}</Text>);

    return (
      <View style={styles.container}>

        <Image style={styles.img} source={require('../assets/icon.png')} />
        <Text style={styles.txt}>Employees {'\n'}</Text>
        <TextInput style={styles.searchInput} onChangeText={this.searchUpdated} placeholder='search employee' />
        {listItems}
        <Modal animationType="slide" transparent={false} visible={this.state.detailsVisible}>
          <Details empID={this.state.id} myView={this.handleOpen} />
          <Button color="#00ff00" title="Back" onPress={() => { this.handleOpen(false, this.state.id); }} />
        </Modal>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  searchInput: {
    padding: 10,
    borderColor: '#CCC',
    borderWidth: 1
  },
  container: {
    flex: 1,
    paddingTop: 22
  },
  sectionHeader: {
    paddingTop: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 2,
    fontSize: 14,
    fontWeight: 'bold',
    backgroundColor: 'rgba(247,247,247,1.0)',
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
  edit: {
    padding: 10,
    fontSize: 18,
    height: 44,
    borderWidth: 1,
    borderColor: '#d3d3d3',
  },
  txt: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: "center"
  },
  img: {
    height: 100,
    width: 100,
    alignSelf: "center"
  },
})

const mapStateToProps = (state, ownProps) => {
  return {
    employees: state.employees,
  }
};

export default connect(mapStateToProps, null)(Employees);