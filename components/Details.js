import React, { PureComponent } from 'react'
import { View, Text, StyleSheet, Image, ScrollView, Button } from 'react-native'
import { connect } from 'react-redux';
import t from 'tcomb-form-native'; // 0.6.9
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import * as employeeAction from '../actions/employeeAction';

const Form = t.form.Form;

const User = t.struct({
  firstname: t.String,
  lastname: t.String,
  position: t.String,
  department: t.String,
  phonenumber: t.Number,
  isActive: t.Boolean
});

class Details extends PureComponent {

  state = {
    form: {
      firstname: '',
      lastname: '',
      position: '',
      department: '',
      phonenumber: 0,
      isActive: false
    },
    myID: 0
  }

  deleteEmployee = (index) => {
    this.props.deleteEmployee(index);
    this.props.myView(false,this.state.myID);
  }

  updateEmployee = (index) => {
    let employees = {
      firstname: this.state.form.firstname,
      lastname: this.state.form.lastname,
      position: this.state.form.position,
      department: this.state.form.department,
      phonenumber: this.state.form.phonenumber,
      isActive: this.state.form.isActive
    }
    this.props.updateEmployee(employees, index);
    this.props.myView(false,this.state.myID);
  }

  onChange = (e) => {
    this.setState(() => {
      return { form: e }
    });
  }

  componentDidMount() {
    this.setState(() => {
      return { form: this.props.employees[this.state.myID] }
    });
  }

  render() {
    this.state.myID = this.props.empID
    return (
      <ScrollView>
        <View >
          <Image style={styles.img} source={require('../assets/icon.png')} />
          <View style={styles.container}>
          <Button color="#00ff00" title="Save" onPress={() => { this.updateEmployee(this.state.myID); }} />
          <Button color="#ff0000" style={{fontSize:50}} title="Delete" onPress={() => { this.deleteEmployee(this.state.myID); }} />
          </View>
          <KeyboardAwareScrollView>
            <Form type={User} ref='form' value={this.state.form} onChange={this.onChange} />
          </KeyboardAwareScrollView>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  img: {
    height: 100,
    width: 100,
    alignSelf: "center"
  },
  container:{
    flexDirection:"row",
    justifyContent:"center"
  },
})
const mapDispatchToProps = (dispatch) => {
  return {
    deleteEmployee: index => dispatch(employeeAction.deleteEmployee(index)),
    updateEmployee: (employees, index) => dispatch(employeeAction.updateEmployee(employees, index)),
  }
};
const mapStateToProps = (state, ownProps) => {
  return {
    employees: state.employees,
  }
};
export default connect(mapStateToProps, mapDispatchToProps)(Details);
